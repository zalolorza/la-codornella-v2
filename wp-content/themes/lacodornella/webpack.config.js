var path = require("path");
var webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
var ini = require("ini");
var fs = require("fs");
var WebpackOnBuildPlugin = require("on-build-webpack");
var CleanWebpackPlugin = require("clean-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");

var entries = {
  build: "./src/main.js",
  admin: "./src/admin.js",
  login: "./src/login.js"
};

if (process.env.NODE_ENV == "development") {
  var buildPublicPath = "/dist/";
} else {
  var buildPublicPath = "/wp-content/themes/lacodornella/dist/";
}

var sassExtract = [{
    loader: "css-loader",
    options: {
      importLoaders: 1
    } // translates CSS into CommonJS modules
  },
  {
    loader: "sass-loader" // compiles Sass to CSS
  },
  {
    loader: "sass-resources-loader",
    options: {
      resources: path.resolve(__dirname, "./src/assets/sass/resources/*.scss")
    }
  }
];

var rules = {
  esLint: {
    enforce: 'pre',
    test: /\.(js|vue)$/,
    loader: 'eslint-loader',
    exclude: /node_modules/
  },

  babel: {
    test: /\.js$/,
    loader: "babel-loader",
    exclude: /node_modules/
  },

  files: {
    test: /\.(png|jpg|gif|svg|otf|eot|woff|ttf|eot?|woff2)$/,
    loader: "file-loader",
    options: {
      name: "[name].[ext]?[hash]"
    }
  },

  sass: {
    test: /\.s[a|c]ss$/,
    use: ExtractTextPlugin.extract({
      use: sassExtract
    })
  },

  vue: {
    test: /\.vue$/,
    loader: "vue-loader",
    options: {
      loaders: {
        scss: ExtractTextPlugin.extract({
          use: sassExtract,
          fallback: "vue-style-loader"
        })
      }
    }
  }
};

module.exports = {
  entry: entries,
  output: {
    path: path.resolve(__dirname, "./dist"),
    publicPath: buildPublicPath,
    filename: `[name].${
      process.env.NODE_ENV === "development" ? "dev" : "[hash]"
      }.js`
  },
  module: {
    rules: [rules.babel, rules.vue, rules.files, rules.sass]
  },
  resolve: {
    alias: {
      '@js': path.resolve(__dirname, "src/assets/js"),
      '@sass': path.resolve(__dirname, "src/assets/sass"),
      '@modules': path.resolve(__dirname, "src/modules"),
      '@images': path.resolve(__dirname, "src/images"),
      vue$: "vue/dist/vue.esm.js",
      TweenLite: path.resolve(
        "node_modules",
        "gsap/src/uncompressed/TweenLite.js"
      ),
      TweenMax: path.resolve(
        "node_modules",
        "gsap/src/uncompressed/TweenMax.js"
      ),
      TimelineLite: path.resolve(
        "node_modules",
        "gsap/src/uncompressed/TimelineLite.js"
      ),
      TimelineMax: path.resolve(
        "node_modules",
        "gsap/src/uncompressed/TimelineMax.js"
      ),
      ScrollMagic: path.resolve(
        "node_modules",
        "scrollmagic/scrollmagic/uncompressed/ScrollMagic.js"
      ),
      "animation.gsap": path.resolve(
        "node_modules",
        "scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js"
      ),
      "debug.addIndicators": path.resolve(
        "node_modules",
        "scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js"
      )
    }
  },
  devServer: {
    https: false,
    historyApiFallback: true,
    noInfo: true,
    port: 8084,
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
      "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
    }
  },
  performance: {
    hints: false
  },
  devtool: "#eval-source-map",
  plugins: [
    new ExtractTextPlugin({
      filename: `[name].${
        process.env.NODE_ENV === "development" ? "dev" : "[hash]"
        }.css`
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: "static",
      filename: `static.vendor.${
        process.env.NODE_ENV === "development" ? "dev" : "[hash]"
        }.js`,
      minChunks(module, count) {
        if (count > 1) {
          var context = module.context;
          return context && context.indexOf("node_modules") >= 0;
        }
      }
    }),
    new WebpackOnBuildPlugin(function (stats) {
      var config = {};
      config.NODE_ENV = process.env.NODE_ENV;

      if (process.env.NODE_ENV === "development") {
        config.SCRIPTS_HASH = "dev";
      } else {
        config.SCRIPTS_HASH = stats.hash;
      }
      if (!fs.existsSync("./dist/")) {
        fs.mkdirSync("./dist/");
      }
      fs.unlink("./dist/scripts.ini", function (err) {
        fs.appendFile("./dist/scripts.ini", ini.stringify(config, {}), function (
          err
        ) {
          if (err) throw err;
          console.log("new build is done: " + config.SCRIPTS_HASH);
        });
      });

      if(config.NODE_ENV === 'production'){
        const glob = require('glob');
        const output = {};
        glob('src/modules/**/acf.json', (error, files) => {
          files.forEach((filename, index) => {
            output[index] = JSON.parse(fs.readFileSync(filename, 'utf8'));
          });
          fs.writeFileSync('./dist/modules_acf.' + stats.hash + '.json', JSON.stringify(output));
          console.log("new acf json is done: " + 'modules_acf.' + stats.hash + '.json');
        });
      }

    })
  ]
};

if (process.env.NODE_ENV === "production") {
  module.exports.devtool = "#source-map";
  // http://vue-loader.vuejs.org/en/workflow/production.html
  module.exports.plugins = (module.exports.plugins || []).concat([
    new CleanWebpackPlugin(["dist"]),
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      compress: {
        warnings: false
      }
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true
    }),
    new CopyWebpackPlugin([{
        from: "src/**/*.twig",
        transformPath(targetPath, absolutePath) {
          var path = targetPath.substr(targetPath.lastIndexOf("/") + 1);
          return path;
        }
      }, {
        from: "src/modules/**/*.php",
        transformPath(targetPath, absolutePath) {
          var filename = targetPath.substr(targetPath.lastIndexOf("/") + 1);
          var dir = targetPath.replace("/"+filename, "");
          dir = dir.substr(dir.lastIndexOf("/") + 1)
          return 'modules/' + dir + '/' + filename;
        }
      },
      {
        from: "src/assets/images/**/*",
        transformPath(targetPath, absolutePath) {
          var path = targetPath.substr(targetPath.lastIndexOf("/") + 1);
          return path;
        }
      }
    ])
  ]);
} else if (process.env.NODE_ENV === "development") {
  module.exports.output.publicPath = module.exports.output.publicPath;
}
