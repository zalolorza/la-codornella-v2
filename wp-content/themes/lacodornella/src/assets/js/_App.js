import Vue from "vue";
//import { mapMutations, mapGetters, mapActions } from "vuex";
//import { store } from "js/store";
//import "js/filters";
//import "js/directives";
import select from "@js/select-dom";

window.render_module = (el, attr = 'js-component', $vm = null) => {
  var name = el.getAttribute(attr)
  var fnInternal = require(`@modules/${name}/${name}.js`)
  fnInternal = fnInternal.default || fnInternal
  if($vm && typeof $vm.mountInternalComponent === 'function'){
    return $vm.mountInternalComponent(el, fnInternal, name)
  } else {
    return fnInternal(el)
  }
}

// starts the magic

class App {
  constructor() {
    this.nodes = [];
    this.init = this.init.bind(this);
  }

  init() {
    this.modules = this.fetchModules();
    this.fetchHtmlTemplates();
    var self = this;

    var VueApp = {
      data() {
        return {
          modulesMounted: false
        };
      },
      mounted() {
        this.mountModules();
      },
      updated() {
        this.mountModules();
      },
      methods: {
        mountModules() {
          if (this.modulesMounted || this.$el.length == 0) return;
          this.modulesMounted = true;
          var modules = select.all("[js-component]", this.$el);
          for (let i = 0; i < modules.length; i++) {
            self.modules.internal[modules[i].dataset.fn](modules[i]);
          }
        }
      },
      computed: {
      }
    };

    for(let i = 0; i < this.modules["component"].length; i++){
      let mod = this.modules["component"][i];
      if(this.modules["mounted-components"].indexOf(mod.name) !== -1) continue;
      Vue.component(mod.name,mod.fn)
      this.modules["mounted-components"].push(mod.name);
    }

    for (let i = 0; i < this.modules["component.vue"].length; i++) {
      var parent = select.selectParent(this.modules["component.vue"][i].el, "[vue-app]");
      if (parent) {
        this.initModule(this.modules["component.vue"][i]);
      } else {
        VueApp.el = this.modules["component.vue"][i].el;
        VueApp.render = h => h(this.modules["component.vue"][i].fn);
        new Vue(VueApp);
        VueApp.render = null;
      }
    }

    for (let i = 0; i < this.modules["vue-app"].length; i++) {
      VueApp.el = this.modules["vue-app"][i].el;
      new Vue(VueApp);
    }

    for (let i = 0; i < this.modules.module.length; i++) {
      this.initModule(this.modules.module[i]);
    }
  }

  initModule(module) {
    if (module.type === "component.vue") {
      //Vue.component(module.name, module.fn);
    } else if (module.type === "component") {
      
    } else if (module.fn && module.fn.init) {
      module.fn.init(module.el);
    } else if (typeof module.fn.bind !== "undefined") {
      var $vm = module.fn(module.el);
      if($vm instanceof Vue){
        if($vm.$el instanceof Comment) {
          if(NODE_ENV === 'development') console.warn(`Module "${module.name }" not rendered. Cannot fetch nested js components`)
          return false;
        }
        select.find($vm.$el, '[js-component]').forEach(el => {
          render_module(el, 'js-component', $vm)
        })
      }
    }
  }

  reset() {
    for (let i = 0; i < this.modules.length; i++) {
      this.modules[i].fn &&
        this.modules[i].fn.destroy &&
        this.modules[i].fn.destroy();
    }
    this.modules = [];
  }

  fetchHtmlTemplates() {
    var templates = select.all("template");
    for (let i = 0; i < templates.length; i++) {
      var dummyEl = document.createElement("html");
      dummyEl.innerHTML = templates[i].innerHTML;
      this.fetchModules(dummyEl);
      templates[i].innerHTML = dummyEl.innerHTML;
    }
  }

  fetchModules(el = null) {
    const nodes = select.all(
      "[vue-app], [js-component], [vue-component], [vue-component-vue]",
      el
    );
    const modules = {
      "vue-app": [],
      "component": [],
      "component.vue": [],
      "module": [],
      "internal": {},
      "mounted-components": [],
    };
    for (let i = 0; i < nodes.length; i++) {
      if (nodes[i].getAttribute("vue-app") != null) {
        var type = "vue-app";
      } else if (nodes[i].getAttribute("js-component") != null) {
        var type = "module";
      } else if (nodes[i].getAttribute("vue-component") != null) {
        var type = "component";
      } else {
        var type = "component.vue";
      }

      switch (type) {
        case "vue-app":
          break;
        case "module":
          var name = nodes[i].getAttribute("js-component");
          break;
        case "component":
          name = nodes[i].getAttribute("vue-component");
          nodes[i].setAttribute("is", name);
          if (nodes[i].getAttribute("not-inline") == null) {
            nodes[i].setAttribute("inline-template", "");
          }
          break;
        case "component.vue":
          name = nodes[i].getAttribute("vue-app");
          nodes[i].setAttribute("is", name);
          break;
      }

      if (type === "vue-app") {
        modules["vue-app"].push({
          el: nodes[i],
          type
        });
      } else {
        switch (type) {
          case "module":
            var fn = require(`@modules/${name}/${name}.js`);
            break;
          case "component":
            var fn = require(`@modules/${name}/${name}.vue.js`);
            break;
          case "component.vue":
            var fn = require(`@modules/${name}/${name}.vue`);
            break;
        }
        modules[type].push({
          fn: fn.default || fn,
          el: nodes[i],
          type,
          name
        });
      }
    }

    for (let i = 0; i < modules["vue-app"].length; i++) {
      var internalModules = select.all("[js-component]", modules["vue-app"][i].el);
      var globalModules = modules["module"].filter(function(curr) {
        return !internalModules.includes(curr.el);
      });
      var internalModules = modules["module"].filter(function(curr) {
        return internalModules.includes(curr.el);
      });
      for (let k = 0; k < internalModules.length; k++) {
        var key = i + "_" + k;
        modules.internal[i + "_" + k] = internalModules[k].fn;
        internalModules[k].el.dataset.fn = key;
      }
      modules["module"] = globalModules;
    }

    return modules;
  }
}

export default new App();
