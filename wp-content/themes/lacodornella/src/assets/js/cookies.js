"use strict";

import * as Cookie from "tiny-cookie";

var Cookies = {
  testLocalStorage() {
    this.supportsLocalStorage = true;
    try {
      const test = "__vue-cookielaw-check-localStorage";
      window.localStorage.setItem(test, test);
      window.localStorage.removeItem(test);
    } catch (e) {
      console.error(
        "Local storage is not supported, falling back to cookie use"
      );
      this.supportsLocalStorage = false;
    }
  },

  set(name, value) {
    value = JSON.stringify(value);

    if (this.supportsLocalStorage) {
      localStorage.setItem(name, value);
    } else {
      Cookie.set(name, value);
    }
  },

  get(name) {
    if (this.supportsLocalStorage) {
      var value = localStorage.getItem(name);
    } else {
      var value = Cookie.get(name);
    }

    if (value == null) return value;

    value = JSON.parse(value);

    return value;
  },

  set_non_persistent(name, value) {
    value = JSON.stringify(value);
    const now = new Date;
    now.setDate(now.getDate() + 1);
    Cookie.set(name, value, { expires: now.toGMTString() });
  },

  get_non_persistent(name) {
    var value = Cookie.get(name);

    if (value == null) return value;

    value = JSON.parse(value);

    return value;
  },

  delete(name) {
    if (this.supportsLocalStorage) {
      localStorage.removeItem(name);
    } else {
      document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:01 GMT;";
    }
  },

  clear() {
    if (this.supportsLocalStorage) {
      localStorage.clear();
    } else {
      var cookies = document.cookie.split(";");
      for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
      }
    }
  }
};

Cookies.testLocalStorage();

export default Cookies;
