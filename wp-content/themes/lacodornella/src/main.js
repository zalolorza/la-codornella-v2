import "@sass/main.scss";

import requireAll from "@js/require-all";
import "@js/lib/modernizr-touchevents";
import 'es6-object-assign/auto'
import app from "@js/_App";
import onScroll from '@modules/scroll-down/scroll-down.js';

requireAll(require.context("@sass", true, /(\_.*?\.scss)/));
requireAll(require.context("@modules", true, /\.scss$/));

document.addEventListener("DOMContentLoaded", () => {
  onScroll();
  app.init();
});
