import Vue from 'vue';
import ClickOutside from 'vue-click-outside'

export default {
    data(){
      return {
        menuIsOpen : false
      }
    },
    methods: {
      toggleMenu(){
        this.menuIsOpen = !this.menuIsOpen
      },
      closeMenu(){
        this.menuIsOpen = false
      }
    },
    directives: {
      ClickOutside
    }
  }