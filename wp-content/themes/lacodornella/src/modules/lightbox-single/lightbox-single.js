import Vue from 'vue'
import Bus from '@js/bus'
import select from '@js/select-dom'

export default el => new Vue({
  el,
  data(){
    return {
      isOpen: false,
      cell: null,
      lastVideoPlaying: null,
      video: null
    }
  },
  methods: {
    open(cell){
      if(window.Video) {
        this.lastVideoPlaying =  window.Video.lastPlaying;
        window.Video.pauseAll();
      }
      this.cell = cell;
      this.isOpen = true;
      this.$nextTick(()=>{
        this.mountInternalComponents(cell)
      })
    },
    close(){
      if(this.video && this.video.playing){
        var isPlaying = true;
      } else {
        var isPlaying = false;
      }
      if(window.Video) window.Video.pauseAll();
      if(this.video) {
        if(isPlaying){
          // this.videoOrigin.play();
        }
        if(this.video.provider === 'vimeo'){
          this.videoOrigin.embed.setCurrentTime(this.video.currentTime)
        } else {
          this.videoOrigin.currentTime = this.video.currentTime
        }
      }
      this.isOpen = false
    },
    mountInternalComponents(cell){

      var $jscomponent = select.find(this.$el, '[lightbox-js-component]')[0]
      
      if(typeof $jscomponent !== 'undefined'){
        this.jsInstance = render_module($jscomponent, 'lightbox-js-component')
      }

      if(window.Video 
        && typeof this.jsInstance !== 'undefined' 
        && typeof this.jsInstance.type != 'undefined' 
        && this.jsInstance.type == 'video'){
          var isPlaying = cell.id === this.lastVideoPlaying;
          this.video = this.jsInstance;
          this.videoOrigin = window.Video.playersById[cell.id];
          this.video.on('ready', event => {
            var plyr = event.detail.plyr
            var currentTime = window.Video.playersById[cell.id].currentTime
            var muted = window.Video.playersById[cell.id].muted;
            plyr.volume = window.Video.playersById[cell.id].volume;
            plyr.muted = muted;
            if(isPlaying) plyr.play()
            if(currentTime > 0){
              if(plyr.provider === 'vimeo'){
                plyr.embed.setCurrentTime(currentTime)
              } else {
                plyr.currentTime = currentTime
              }
            }
            if(!isPlaying && plyr.provider === 'vimeo'){
              plyr.play();
              plyr.muted = true;
              var oncePlay = e => {
                e.detail.plyr.muted = muted
                e.detail.plyr.pause();
                e.detail.plyr.off('play', oncePlay)
              }
              plyr.on('play', oncePlay)
            }
          });
        
          
          window.Video.pauseOthers(this.jsInstance);
          setTimeout(()=>{window.Video.pauseOthers(this.jsInstance)}, 600)
      }
    }
  },
  mounted(){
    Bus.$on('LightboxSingle.open', this.open)
    Bus.$on('LightboxSingle.close', this.close)
    select.all('[data-lightbox-single]').forEach(el => {
      var data = JSON.parse(el.getAttribute('data-lightbox-single'))
      if(!data.type) data.type = 'image'
      el.onclick = () => {
        this.open(data)
      }
    })
  }
})