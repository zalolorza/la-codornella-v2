<?php

use LaCodornella\Managers\Menus;

$top = Menus::get_menu('top');

$domain = get_home_url();

foreach($top as $link){
  if(strpos($link->url, $domain) === false) {
    $link->target='_blank';
  } else {
    $link->target='_self';
  }
}

return array(
  'top' => $top,
  'main' => Menus::get_menu('main')
);