const viewport = document.getElementsByClassName("base-layout__content")[0]


export default el => {

  const headerHeight = el.offsetHeight;

  viewport.onscroll = () => {

    if(viewport.scrollTop > headerHeight){
      el.classList.add("has-scrolled")
    } else {
      el.classList.remove("has-scrolled")
    }

  }
}