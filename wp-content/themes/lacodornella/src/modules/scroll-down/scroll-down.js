import Scroll from 'scroll'
import Ease from 'ease-component'


class scrollDown{

  constructor(el){

    this.viewport = {
      'lg': document.getElementsByClassName("base-layout__content")[0],
      'sm': document.getElementsByClassName("base-layout__main")[0]
    }
    
    if(el){

      this.el = el;

      this.target = document.getElementsByClassName('block')[1];

      var scrollTop = 150

      this.el.onclick = () => {
  
        this.scrollTo(this.viewport['lg'])
  
        this.scrollTo(this.viewport['sm'])
        
      }
    } else {

      this.el = document.getElementsByTagName("body")[0]

      scrollTop = 300

    }

    this.viewport['lg'].addEventListener('scroll', () => this.onScroll(this.viewport['lg'], scrollTop))
  
    this.viewport['sm'].addEventListener('scroll', () =>  this.onScroll(this.sviewport['sm'], scrollTop))

  }

  onScroll(viewport, scrollTop = 200) {
  
    if(viewport.scrollTop > scrollTop){
      this.el.classList.add("has-scrolled")
    } else {
      this.el.classList.remove("has-scrolled")
    }
  }
  
  scrollTo(viewport){

    var pos = this.target.getBoundingClientRect().top - viewport.getBoundingClientRect().top + viewport.scrollTop + 3
      
    Scroll.top(viewport, parseInt(pos), { 
        duration: 400, 
        ease: Ease.outBack 
      })
  }
}


export default el => new scrollDown(el)