import Plyr from 'plyr';

window.Video = {
  players: [],
  playersById: {},
  lastPlaying: null,
  pauseAll: () => {
    window.Video.players.forEach(player => {
      player.pause()
    })
  },
  pauseOthers: instance => {
    window.Video.players.forEach(player => {
      if(instance !== player) player.pause()
    })
  }
}

export default (el) => {
  var id = el.getAttribute('data-plyr-embed-id');
  var player = new Plyr(el, {
    settings: [],
    fullscreen: {
      enabled: false
    },
    youtube: { 
      noCookie: true, 
      rel: 0, 
      showinfo: 0, 
      iv_load_policy: 3, 
      modestbranding: 1,
      color:'white',
      controls: 0
    }
  })
  player.on('ready', event => {
    if(typeof window.Video.playersById[id] === 'undefined') window.Video.playersById[id] = event.detail.plyr
  });
  player.on('playing', event => {
    window.Video.pauseOthers(event.detail.plyr)
    window.Video.lastPlaying = id
  });
  player.on('pause', event => {
    if(window.Video.lastPlaying === id) window.Video.lastPlaying = null
  });
  player.on('ended', event => {
    if(window.Video.lastPlaying === id) window.Video.lastPlaying = null
  });
  window.Video.players.push(player);
  return player;
} 