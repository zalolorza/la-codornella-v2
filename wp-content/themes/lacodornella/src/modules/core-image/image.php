<?php

use LaCodornella\Managers\Gutenberg;

$is_full_size = Gutenberg::is_style($context, 'full');

if(isset($context['post'])){
  $post = $context['post'];
  if($post->post_type === 'post' || $post->_wp_page_template === 'services'){
    $is_full_size = false;
  }
}

if($is_full_size){
  $image_size = 'full-landscape';
} else {
  $image_size = 'contained-portrait';
}

$image = wp_get_attachment_image_src($context['attrs']['id'], $image_size);

if($image[1] > $image[2]){
  $image = wp_get_attachment_image_src($context['attrs']['id'], 'contained-landscape');
}

return array(
  'src' => $image[0],
  'lightbox' => wp_get_attachment_image_src($context['attrs']['id'], 'lightbox')[0],
  'width' => $image[1],
  'height' => $image[2],
  'is_full' => $is_full_size
);