import Cookies from 'js-cookie'
import Bus from '@js/bus'

window.ColorScheme = {
  dark(){
    Cookies.set('color-scheme', 'dark')
    document.body.classList.add("inverted-colors")
    document.body.classList.remove("inverted-colors-false")
    Bus.$emit('ColorScheme.changeDark', true)
  },
  light(){
    Cookies.set('color-scheme', 'light')
    document.body.classList.remove("inverted-colors")
    document.body.classList.add("inverted-colors-false")
    Bus.$emit('ColorScheme.changeDark', false)
  }
}

export default (el) => {
  if(!Cookies.get('color-scheme')){
    let compStyles = window.getComputedStyle(el);
    if(compStyles.getPropertyValue('background-color') === 'rgb(63, 82, 255)'){
      window.ColorScheme.dark()
    } else {
      window.ColorScheme.light()
    }
  }
}