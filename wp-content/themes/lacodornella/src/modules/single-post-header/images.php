<?php

$thumbnail = $context['post']->thumbnail;

if(!$thumbnail) return false;

$images = array(
  array(
    'src' => $thumbnail->src('contained-landscape'),
    'lightbox' => $thumbnail->src('lightbox')
  )
);

$gallery = get_field('gallery');

if($gallery){

  foreach($gallery as $image){
    $images[] = array(
      'src' => $image['sizes']['contained-landscape'],
      'lightbox' => $image['sizes']['lightbox']
    );
  }

}

return $images;