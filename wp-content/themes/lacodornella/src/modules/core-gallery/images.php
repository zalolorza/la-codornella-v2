<?php

$images = array();

foreach($context['attrs']['ids'] as $id){
  $images[] = array(
    'src' =>  wp_get_attachment_image_src($id, 'full-landscape')[0],
    'lightbox' => wp_get_attachment_image_src($id, 'lightbox')[0]
  );
}


return $images;