<?php

use Timber\Image;

$context['post']->thumbnail = new Image($context['post']->_thumbnail_id);
return $context['post'];