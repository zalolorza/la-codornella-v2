import requireAll from "@js/require-all";

import "@sass/admin.scss";

requireAll(require.context("@sass/admin", true, /\.scss$/));

requireAll(require.context("@js/admin", true, /\.js$/));

requireAll(require.context("@modules", true, /^(?=.*acf-)(?=.*.scss$).*$/));