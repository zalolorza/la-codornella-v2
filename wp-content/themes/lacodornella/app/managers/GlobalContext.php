<?php

namespace LaCodornella\Managers;

/**
 * Parse & Hydrate data before rendering the right twig
 */

class GlobalContext {

    public function __construct() {
      add_filter('twig_context', array($this, 'add_context'));
    }

    public function add_context($context){
      $context['current_year'] = date("Y");
      $context['template_directory_uri'] = get_template_directory_uri();
      if ( NODE_ENV == 'development'  ) {
        $context['images_dir'] =  get_template_directory_uri().'/src/assets/images';
      } else {
        $context['images_dir'] =  get_template_directory_uri().'/dist';
      }
      return $context;
    }
    
}