<?php

namespace LaCodornella\Managers;
use Lib\TwigFunctions;
use Timber\Twig_Function;
use Lib\Modules;

class Gutenberg {


	public function __construct() {

		add_filter( 'allowed_block_types', array($this,'allowed_block_types') );
		add_action( 'init', array($this,'extend_blocks') );
		add_action('acf/init', array($this,'register_acf_block_types'));
		add_filter( 'timber/twig', array($this, 'add_twig_functions') );

	}

	public function add_twig_functions($twig){
		$twig->addFunction( new Twig_Function( 'the_content', array($this, 'the_content') ) );
		return $twig;
	}

	public function the_content($context = array()){
		global $VIEW;
		TwigFunctions::add_context($context);
		echo Gutenberg::parse_blocks($VIEW->context['post']->post_content);
	}

	function register_acf_block_types(){
		acf_register_block_type(array(
			'name'              => 'post-card',
			'title'             => __('Targeta pàgina'),
			'description'       => __('Drecera cap a pàgina'),
			'render_callback'		=> array($this,'render_block'),
			'category'          => 'widgets',
			'icon'              => 'index-card',
			'keywords'          => array( 'card', 'page' ),
		));
	}

	function extend_blocks(){
		/*register_block_style(
			'core/gallery',
			array(
					'name'         => 'slideshow',
					'label'        => __( 'Slideshow' )
			)
		);*/

		register_block_style(
			'core/image',
			array(
					'name'         => 'full',
					'label'        => __( 'Full' )
			)
		);
	}

	function allowed_block_types( $allowed_blocks ) {
 
		return array(
			'core/paragraph',
			'core/heading',
			'core/image',
			'core/gallery',
			'core-embed/youtube',
			'core-embed/vimeo',
			'acf/post-card'
		);
	 
	}

	static function is_style($block, $style){
		if(!isset($block['attrs']['className']) || strpos($block['attrs']['className'],'is-style-'.$style) === false){
			return false;
		} else {
			return true;
		}
	}

	static function parse_blocks($content){
		$html = '';

		foreach(parse_blocks($content) as $block){
			if(!isset($block['blockName'])) continue;
			if(strpos($block['blockName'], 'acf/') !== false) {
				acf_setup_meta( $block['attrs']['data'], $block['attrs']['id'], true );
				$block = array_merge($block, get_fields());
				acf_reset_meta( $block['attrs']['id'] );
			}
			$html .= self::render_block($block);
		}

		return $html;
	}

	static function render_block($block, $content = '', $is_preview = false, $post_id = 0 ){
		$name = isset($block['blockName']) ? $block['blockName'] : $block['name'];
		$name = str_replace('/','-',$name);
		$block['block_class'] = $name;
		if($is_preview) {
			$block = array_merge($block, get_fields());
		}
		$html = Modules::get_module($name, $block);
		if($is_preview) echo $html;
		return $html;
	}


}
