<?php

/**
 *
 * Autoload composer dependencies
 *
 */
include_once( dirname(__DIR__) . '/vendor/autoload.php' );

/**
 *
 * Autoload all helpers and utils
 *
 */
/* foreach (glob(__DIR__ . "/helpers/*.php") as $filename){
            include_once $filename;
        } */

/**
 *
 *  Spl autoload
 *
 */

spl_autoload_register(function($class) {
  $namespace = 'LaCodornella\\';

  $length = strlen($namespace);
  $base_directory = __DIR__.'/';

  if(strncmp($namespace, $class, $length) === 0){
    $dir = str_replace('\\', '/',  substr($class, $length));
  } else {
    return;
  } 
  
  $path = explode('/',$dir);
  if(sizeof($path)>1){
    $realpath = array();
    foreach ($path as $i => $chunk) {
      if($i == sizeof($path)-1){
        $realpath[] = $chunk;
      } else {
        $realpath[] = str_replace('_', '-',  strtolower($chunk));
      }
    }
    $dir = implode('/',$realpath);
  }
  
  $file = $base_directory . $dir . '.php';
  if(file_exists($file)) {
    require_once $file;
  }
});
