<?php

namespace CurialTirant\API\REST;
use CurialTirant\Managers\PostData;

/**
 * Rest API post response class
 */

class Post {

  function __construct(){
      add_filter( 'rest_prepare_post', array($this,'json_prepare_post'), 10, 3 );
      add_filter( 'rest_post_query', array($this,'posts_order'), 10, 2  );
  }


	public function posts_order($args, $request){
    $args['orderby'] = 'menu_order';
    $args['order'] = 'asc';
  	return $args;
	}

  function json_prepare_post($data, $post, $request){
    $params = $request->get_params();
    if ( isset( $params['id'] ) ) return $data;
  
    $data->data = PostData::prepare($data->data);

    return $data;
  }


}

