<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'lacodornella' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define('WP_POST_REVISIONS', 3);
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */

define('AUTH_KEY',         ')= SlNR4>xfXTdhO{p^x}0P:3B@oySoM}In K5}({§Z.DH},U`5j2cY1kC(J[H}.');
define('SECURE_AUTH_KEY',  'u5]j]f.hi@7]>LOJK/e)^z3v CMat@T/xJEW/prY9BSF%[c@AuKU}&t{R!rEFQ53');
define('LOGGED_IN_KEY',    'S@(S5ue=rdl?a c!_m7>_/V§ ^NLnMl2kFXVjd@]T$tg0BnP2-Q$?70Gyk~ck~.N');
define('NONCE_KEY',        'jQX$AHg<J./ac-8qfnH5;+|m.rk39U}T-@yuzuawtp1JphLr=M,$tYGoLIXP@aRF');
define('AUTH_SALT',        '2Rd`i>!C!+[|XAiB6_BNuJmyj0BwL7U4% FEfW3O6TYES%|ABU !1n?P9Mt9oui5');
define('SECURE_AUTH_SALT', 'Gyq^|>k8vhsyF{4n§K![aH<u1@`DPY§/!+nP.Tf$0J5$$Xd&jf@UQX<hEdfRcf}(');
define('LOGGED_IN_SALT',   '3KH$9_N[:jn44[^^>^Dm&Ob|8RDhs-sh?W+LZgs!ie^QdfVFa[ MtNb&WnVL0S 3');
define('NONCE_SALT',       'V+b7^EUFxBXb@(@uV]_aAxMDKhC9%Qe](Y0Vx7-Q64xt{M4%<&l3EKJv=3f:?5%v');



/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
